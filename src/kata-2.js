export function kata2(obj, def, path) {
    const pathReducer = function (path) {
        const elem = path.split('.').reduce((o, i) => {
            if (o[i]) return o[i];
            return false;
        }, obj);
        if (!elem) return def;
        return elem;
    };

    if (!path) return pathReducer;

    return pathReducer(path);

}