export function kata3(n, m) {
    let selectedNumbers = [];
    for (let i = n; i <= m; i++) {
        let dividers = [];
        for (let x = i; x >= 1; x--) {
            if (i % x == 0) {
                dividers.push(x);
            }
        }
        let dividersSum = dividers.reduce((acc, current) => acc + (current * current), 0);
        if (Math.sqrt(dividersSum) % 1 === 0) {
            selectedNumbers.push([i, dividersSum]);
        }

    }
    return selectedNumbers;

}