export function kata1(number) {

    function compare(a, b) {
        if (a > b) return -1;
        if (a < b) return 1;
        return 0;
    }
    return parseInt((number).toString(10).split('').sort(compare).join(''));

}